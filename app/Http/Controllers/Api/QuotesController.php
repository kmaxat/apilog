<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Mail\DeliveryRequest;
use App\Models\Quote;
use App\Models\Website;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;

class QuotesController extends Controller
{
    public function store(Request $request)
    {
        $this->validate($request, [
            'to' => 'required',
            'from' => 'required',
            'weight' => 'required',
        ]);
        $website = Website::where('url', $request->header('origin'))->first();
        if (!$website) {
            $website = Website::create([
                'url' => $request->header('origin'),
            ]);
        }
        $quote = Quote::create([
            'quantity' => $request->get('quantity'),
            'type' => $request->get('type'),
            'name' => $request->get('name'),
            'weight' => $request->get('weight'),
            'width' => $request->get('width'),
            'length' => $request->get('length'),
            'height' => $request->get('height'),
            'from_city' => $request->input('from.city.name'),
            'from_address' => $request->input('from.address'),
            'from_description' => $request->input('from.description'),
            'to_city' => $request->input('to.city.name'),
            'to_address' => $request->input('to.address'),
            'to_description' => $request->input('to.description'),
            'price' => $request->get('price'),
            'temperature' => $request->get('temperature'),
            'incoterms' => $request->get('incoterms'),
            'insurance' => $request->get('insurance'),
            'website_id' => $website->id,
            'user_agent' => $request->header('user-agent'),
            'quote_type' => 'calculation',
            'email' => $request->get('email'),
            'telephone' => $request->get('telephone'),
            'code' => $request->get('code'),
            'volume' => $request->get('volume'),

        ]);

        $ids = $this->getExlineIds($request);
        $quotes = [];
        if ($ids) {
            $quotes = $this->fetchExlineQuotes($ids, $request,$quotes);
        }
        $quotes[] = [
            'type' => 'request',
            'price' => '',
            'company' => 'Universal',
            'human_range' => '',
            'max' => '',
            'min' => '',
            'desc' => 'Прайс будет доставлен в течение 5-ти часов'
        ];
        return $quotes;
    }

    private function fetchExlineQuotes($ids, $request, $quotes)
    {
        $client = new \GuzzleHttp\Client();
        $res = $client->request('GET', 'https://api.exline.systems/public/v1/calculate', [
            'query' => [
                'origin_id' => $ids['from'],
                'destination_id' => $ids['to'],
                'weight' => $request->get('weight'),
                'w' => $request->get('width'),
                'l' => $request->get('length'),
                'h' => $request->get('height')
            ]
        ]);
        $response = json_decode($res->getBody(), true);
        foreach ($response['calculations'] as $key =>$value) {
            if (isset($value['price'])) {
                if($value['max'] == 'No data' || $value['min'] == 'No data'){
                    $value['max'] = 'Нет данных';
                    $value['min'] = 'Нет данных';
                }
                $quotes[] = [
                    'type' =>  $key,
                    'price' => $value['price'],
                    'company' => 'Exline',
                    'human_range' => $value['human_range'],
                    'max' => $value['max'],
                    'min' => $value['min'],
                    'desc' => ''
                ];
            }
        }
        return $quotes;
    }
    
    private function getExlineIds($request)
    {
        $fromId = $this->getExlineId($request->input('from.city.name'));
        if ($fromId) {
            $toId = $this->getExlineId($request->input('to.city.name'));
            if ($toId)
                return [
                    'from' => $fromId,
                    'to' => $toId
                ];
        }
        return false;

    }

    private function getExlineId($name)
    {
        $city = explode(',', $name);
        $client = new \GuzzleHttp\Client();
        $res = $client->request('GET', 'https://api.exline.systems/public/v1/regions/destination?title=' . $city[0]);

        $regions = json_decode($res->getBody(), true);
        if ($regions['meta']['total'] < 1 || !is_array($regions['regions'])) {
            return false;
        }
        return $regions['regions'][0]['id'];
    }
}

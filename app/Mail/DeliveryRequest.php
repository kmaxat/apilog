<?php

namespace App\Mail;

use App\Models\Quote;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class DeliveryRequest extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public $quote, $company, $delivery_type;

    public function __construct(Quote $quote, $company, $delivery_type)
    {
        $this->quote = $quote;
        $this->company = $company;
        $this->delivery_type = $delivery_type;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        if ($this->company == 'Universal') {
            return $this->from('noreply@apilog.kz')->to([
                'api@apilog.kz',
                't.rustam19@gmail.com'
            ])->subject('Запрос на оценку доставки')->view('emails.order');
        }
        return $this->from('noreply@apilog.kz')->to([
            'api@apilog.kz',
            'orca@exline.kz'
        ])->subject('Запрос на оценку доставки')->view('emails.order');
    }
}
